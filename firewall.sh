#!/bin/bash
# iptables menu service ax
# 0.3 version testing 

if [ "$(id -u)" != "0" ]; then
	echo    
	echo "Sono richiesti i privilegi di root" 1>&2
   	echo
	exit 1
fi

############################### IPTABLE SERVICE CHECKSTATUS ###############################
checkstatus()
 {
 opt_checkstatus=1
 while [ $opt_checkstatus != 7 ]
      do
       clear
  echo -e "\n Note: Salva le tue regole Iptables prima dello Stop/Start/Restart del servizio "
  echo -e "       potrai visionarl in un secondo momento [iptables.rules]                  \n"
  echo -e "   Iptables Menu:\n"
  echo -e "   1. Salva le regole di [iptables]\n
   2. Status\n
   3. Start *\n
   4. Stop *\n
   5. Restart *\n
   6. Flush+Reset * (Flush+Reset di tutte le regole del firewall in uso)\n
   7. Torna al Menu principale"
  echo -e ""
   read opt_checkstatus
  case $opt_checkstatus in
   1) echo -e "*****************************************************************" 
               iptables-save > /etc/iptables/iptables.rules
      echo -e "iptables-save > /etc/iptables/iptables.rules\n"
      echo -e "FATTO"
      echo -e "*****************************************************************\n"
    				 echo -e "Premi un tasto per continuare..."
    				 read temp;;
   2) echo -e "*****************************************************************"
               systemctl status iptables
      echo -e "*****************************************************************\n"
                                 echo -e "Premi un tasto per continuare..."
                                 read temp;;
   3) echo -e "*****************************************************************"  
               systemctl start iptables 
               systemctl status iptables
      echo -e "*****************************************************************\n"
                                 echo -e "Premi un tasto per continuare..."
                                 read temp;;
   4) echo -e "*****************************************************************"
               systemctl stop iptables
               systemctl status iptables
      echo -e "*****************************************************************\n"
                                 echo -e "Premi un tasto per continuare..."
                                 read temp;;     
   5) echo -e "*****************************************************************"
               systemctl restart iptables
               systemctl status iptables
      echo -e "*****************************************************************\n"
                                 echo -e "Premi un tasto per continuare..."
                                 read temp;;
   6)           ## set default policies to let everything in
                iptables --policy INPUT   ACCEPT;
                iptables --policy OUTPUT  ACCEPT;
                iptables --policy FORWARD ACCEPT;
                ##  
                ## start fresh
                iptables -Z; # zero counters
                iptables -F; # flush (delete) rules
                iptables -X; # delete all extra chains
      echo -e "******************************************************************"
      echo -e "Tutte le regole di [iptables] sono state Resettate/Flushate!!!"
      echo -e "******************************************************************\n"
                                  echo -e "Premi un tasto per continuare..."
                                  read temp;;
   7) clear;;
      #show_menu;;
   *) echo -e "Opzione selezionata inesistente!"
  esac
 done
 }

############################### IPTABLE SERVICE CHECKSTATUS6 ###############################
checkstatus6()
 {
  opt_checkstatus6=1
 while [ $opt_checkstatus6 != 7 ]
      do
       clear
  echo -e "\n Note: Salva le tue regole [ip6tables] prima dello Stop/Start/Restart del servizio "
  echo -e "       potrai visionarl in un secondo momento [ip6tables.rules]                  \n"
  echo -e "   Ip6tables Menu:\n"
  echo -e "   1. Salva le regole di [ip6tables]\n
   2. Status ipv6\n
   3. Start 6*\n
   4. Stop 6*\n
   5. Restart 6*\n
   6. Flush+Reset 6* (Flush+Reset di tutte le regole del firewall ipv6 in uso)\n
   7. Torna al Menu principale"
  echo -e ""
   read opt_checkstatus6
  case $opt_checkstatus6 in
   1) echo -e "*****************************************************************" 
               ip6tables-save > /etc/iptables/ip6tables.rules
      echo -e "ip6tables-save > /etc/iptables/ip6tables.rules\n"
      echo -e "FATTO"
      echo -e "*****************************************************************\n"
                                 echo -e "Premi un tasto per continuare..."
                                 read temp;;
   2) echo -e "*****************************************************************"
               systemctl status ip6tables
      echo -e "*****************************************************************\n"
                                 echo -e "Premi un tasto per continuare..."
                                 read temp;;
   3) echo -e "*****************************************************************"  
               systemctl start ip6tables 
               systemctl status ip6tables
      echo -e "*****************************************************************\n"
                                 echo -e "Premi un tasto per continuare..."
                                 read temp;;
   4) echo -e "*****************************************************************"
               systemctl stop ip6tables
               systemctl status ip6tables
      echo -e "*****************************************************************\n"
                                 echo -e "Premi un tasto per continuare..."
                                 read temp;;     
   5) echo -e "*****************************************************************"
               systemctl restart ip6tables
               systemctl status ip6tables
      echo -e "*****************************************************************\n"
                                 echo -e "Premi un tasto per continuare..."
                                 read temp;;
   6)           ## set default policies to let everything in
                ip6tables --policy INPUT   ACCEPT;
                ip6tables --policy OUTPUT  ACCEPT;
                ip6tables --policy FORWARD ACCEPT;
                ##  
                ## start fresh
                ip6tables -Z; # zero counters
                ip6tables -F; # flush (delete) rules
                ip6tables -X; # delete all extra chains
      echo -e "******************************************************************"
      echo -e "Tutte le regole di [ip6tables] sono state Resettate/Flushate!!!"
      echo -e "******************************************************************\n"
                                  echo -e "Premi un tasto per continuare..."
                                  read temp;;
   7) clear;;
      #show_menu;;
   *) echo -e "Opzione selezionata inesistente!"
  esac
 done
 }

############################### Opzioni di costruzione del firewall ############################### 
buildfirewall()
 {
  ###############Getting the Chain############
  echo -e "Using Which Chain of Filter Table?\n
  1. INPUT
  2. OUTPUT
  3. Forward"
  read opt_ch
  case $opt_ch in
   1) chain="INPUT" ;;
   2) chain="OUTPUT" ;;
   3) chain="FORWARD" ;;
   *) echo -e "Wrong Option Selected!!!"
  esac
 
  #########Getting Source IP Address##########
  #Label
   
  echo -e "
  1. Firewall using Single Source IP\n
  2. Firewall using Source Subnet\n
  3. Firewall using for All Source Networks\n"
  read opt_ip
   
  case $opt_ip in
   1) echo -e "\nPlease Enter the IP Address of the Source"
   read ip_source ;;
   2) echo -e "\nPlease Enter the Source Subnet (e.g 192.168.10.0/24)"
   read ip_source ;;
   3) ip_source="0/0" ;;
   #4) ip_source = "NULL" ;;
   *) echo -e "Wrong Option Selected"
  esac
  #########Getting Destination IP Address##########
   echo -e "
  1. Firewall using Single Destination IP\n
                2. Firewall using Destination Subnet\n
         3. Firewall using for All Destination Networks\n"
  
     read opt_ip
              case $opt_ip in
        1) echo -e "\nPlease Enter the IP Address of the Destination"
                     read ip_dest ;;
               2) echo -e "\nPlease Enter the Destination Subnet (e.g 192.168.10.0/24)"
                     read ip_dest ;;
               3) ip_dest="0/0" ;;
        #4) ip_dest = "NULL" ;;
               *) echo -e "Wrong Option Selected"
       esac
       ###############Getting the Protocol#############
       echo -e "
       1. Block All Traffic of TCP
       2. Block Specific TCP Service
       3. Block Specific Port
       4. Using no Protocol"
       read proto_ch
       case $proto_ch in
        1) proto=TCP ;;
        2) echo -e "Enter the TCP Service Name: (CAPITAL LETTERS!!!)"
       read proto ;;
        3) echo -e "Enter the Port Name: (CAPITAL LETTERS!!!)" 
       read proto ;;
        4) proto="NULL" ;;
        *) echo -e "Wrong option Selected!!!"
       esac
 
       #############What to do With Rule############# 
       echo -e "What to do with Rule?
       1. Accept the Packet
       2. Reject the Packet
       3. Drop the Packet
       4. Create Log"
       read rule_ch
       case $rule_ch in 
        1) rule="ACCEPT" ;;
        2) rule="REJECT" ;;
        3) rule="DROP" ;;
        4) rule="LOG" ;;
       esac
###################Generating the Rule####################
echo -e "\n\tPress Enter key to Generate the Complete Rule!!!"
read temp
echo -e "The Generated Rule is \n"
if [ $proto == "NULL" ]; then
 echo -e "\niptables -A $chain -s $ip_source -d $ip_dest -j $rule\n"
 gen=1
else
 echo -e "\niptables -A $chain -s $ip_source -d $ip_dest -p $proto -j $rule\n"
 gen=2
fi 
echo -e "\n\tDo you want to Enter the Above rule to the IPTABLES? Yes=1 , No=2"
read yesno
if [ $yesno == 1 ] && [ $gen == 1 ]; then
 iptables -A $chain -s $ip_source -d $ip_dest -j $rule
else if [ $yesno == 1 ] && [ $gen == 2 ]; then
 iptables -A $chain -s $ip_source -d $ip_dest -p $proto -j $rule         
   
else if [ $yesno == 2 ]; then
 
 show_menu
fi
fi
fi
}

show_menu(){
NORMAL=`echo "\033[m"` # bianco
MENU=`echo "\033[36m"` # azzurro
RED_TEXT=`echo "\033[1;31m"` # rosso light
NUMBER=`echo "\033[33m"` # arancio
echo 
echo -e "${MENU}|==============================|${NORMAL}"
echo -e "${MENU}|                              |${NORMAL}"
echo -e "${MENU}|=*${NORMAL}   [ax] Firewall Menù     ${MENU}*=|${NORMAL}"
echo -e "${MENU}|                              |${NORMAL}"
echo -e "${MENU}|- ${NUMBER}1.${NORMAL}Visualizza fw ipv4        ${MENU}|${NORMAL}"
echo -e "${MENU}|- ${NUMBER}2.${NORMAL}Visualizza fw ipv6        ${MENU}|${NORMAL}${NORMAL}"
echo -e "${MENU}|                              ${MENU}|${NORMAL}"
echo -e "${MENU}|- ${NUMBER}3.${NORMAL}Status/check [iptables]   ${MENU}|${NORMAL}" 
echo -e "${MENU}|- ${NUMBER}4.${NORMAL}Status/check [ip6tables]  ${MENU}|${NORMAL}" 
echo -e "${MENU}|                              ${MENU}|${NORMAL}"
echo -e "${MENU}|- ${NUMBER}5.${NORMAL}Costruisci regole ipv4    ${MENU}|${NORMAL}"
echo -e "${MENU}|- ${NUMBER}6.${NORMAL}Costruisci regole ipv6    ${MENU}|${NORMAL}"
echo -e "${MENU}|                              ${MENU}|${NORMAL}"
echo -e "${MENU}|- ${NUMBER}7.${RED_TEXT}ESCI${NORMAL}                      ${MENU}|${NORMAL}"
echo -e "${MENU}|                              ${MENU}|${NORMAL}"
echo -e "${MENU}|==============================|${NORMAL}"
echo
read -p "Scegli: " menu
}

clear
show_menu
while [ menu != '' ]
    do
    if [[ $menu = "" ]]; then 
            exit;
    else
case $menu in 	
		1) clear;
		iptables -L | less;
		;;
	
		2) clear;
		ip6tables -L | less;
		;;
		
		3) clear;
                checkstatus;
		;;
	
		4) clear;
		checkstatus6;
		;;
	
		5) clear;
		buildfirewall;
		;;
	
		6) clear;
		#buildfirewall6;
		echo -e "ATTENIONE: non è stato ancora configurato buildfirewall6";
		;;
	
		7)	
		echo -e "# END";
		exit 1; 
		;;
		
		*)clear;
		echo -e "ATTENZIONE: devi scegliere una delle opzioni esistenti";
		;;
esac
fi 
show_menu
done
