#Hurricane Electric tunnel broker
#
#ax(ax@slackware.eu)
#
#VAR - modify settings
iface="gif0" #create ipv6 iface 
my_eth0="em0" #bsd eth0 iface 
myipv4=$(ifconfig $my_eth0 | sed -n '/.inet /{s///;s/ .*//;p;}') #search my NAT ipv4
server4="216.x.x.x" #route ipv4 server HE
my6="2001:470:FFFF:FFFF::2" #my ipv6 /128
my6route="2001:470:FFFF:FFFF::1"  #route ipv6 /128

#VAR - Allocated Subnet HE 
#modify with your subnets
#my64="your_64"
my48="2001:470:FFFF::"
my128="2001:470:FFFF::a1c5"
#my128-1="2001:FFFF:FFFF:FFFF:FFFF:FFFF:FFFF:FFFF" other /128

ifconfig $iface create
ifconfig $iface tunnel $myipv4 $server4
ifconfig $iface inet6 $my6
route -n add -inet6 default $my6route

#add routed ::/48 /128 
ifconfig $my_eth0 inet6 $my48 prefixlen 48
ifconfig $my_eth0 inet6 $my128 prefixlen 128
#add other prefix /128
#ifconfig $my_eth0 inet6 $my128-1 prefixlen 128
