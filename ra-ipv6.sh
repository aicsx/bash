#!/bin/bash
#
# ra-ipv6.sh revisione 0.2
# Script che genera un prefisso random.
# Le variabili iniziali vanno modificate in base al prefisso da generare 
# e tutte le modifiche vanno fatte prima di lanciare lo script.
# L'unica eccezione è per "LOCAL" alla riga 61.
# Nel caso specifico viene assegnata una /80 all'user con ipv4.
#
# ax - ax@slackware.eu

#VAR
array=( 1 2 3 4 5 6 7 8 9 0 a b c d e f )
MAXCOUNT=1 # numero di ipv6 da generare alla fine
count=1
network=2001:FFFF:FFFF:a1c5 # ipv6 network prefix

rnd_ip_block ()
{
    a=${array[$RANDOM%16]}${array[$RANDOM%16]}${array[$RANDOM%16]}${array[$RANDOM%16]}
#   b=${array[$RANDOM%16]}${array[$RANDOM%16]}${array[$RANDOM%16]}${array[$RANDOM%16]}
#   c=${array[$RANDOM%16]}${array[$RANDOM%16]}${array[$RANDOM%16]}${array[$RANDOM%16]}
#   d=${array[$RANDOM%16]}${array[$RANDOM%16]}${array[$RANDOM%16]}${array[$RANDOM%16]}
    echo $network:$a::	
#   echo $network:$a:$b:$c:$d
}
#END VAR

#START
echo "$MAXCOUNT IPv6"
echo "*------------------------*"
while [ "$count" -le $MAXCOUNT ]        
do
        rnd_ip_block
        let "count += 1"                
        done > sub.ipv6
	cat sub.ipv6 
echo "*------------------------*" 
echo ""
echo "E' stato creato il file: 'sub.ipv6' con il prefisso generato."

# istruzioni per continuare dopo!
echo ""
echo -n "Vuoi creare lo script di delega con il prefisso generato? [yes or no]: "
read scelta
case $scelta in

        [yY] | [yY][Ee][Ss] )
                echo ""
		echo "Ok procedo, ti farò un paio di domande."
		echo ""
		# Avvio script interattivo con i dati della delega 
		read -p "Quale interfaccia vuoi creare? (nome del tunnel): " iface
		read -p "Qual'è end-point? (ipv4 dell'user): " remote
		echo "Perfetto! procedo con la creazione dello script"
		# Creo il file config con tutti i dati all'interno
		touch $iface.conf
		echo -e "#!/bin/bash" > $iface.conf
		echo -e "" >> $iface.conf
                # Cerco e setto la variabile per l'ipv4 locale della macchina. Va modificato il dev del local ipv4 ovviamente da cui partono le deleghe! Ex. eth0,eth1 ... 
		LOCAL=$(ip -o addr show dev wlp3s0f0 | sed -e's/^.*inet \([^ ]*\)\/.*$/\1/;t;d')
		# Aggiungo le variabile per la subnet creata in precedenza
		SUBNET=$(cat sub.ipv6 | sed -e 's/$/\/80/g')
		ADDRESS=$(cat sub.ipv6 | sed -e 's/$/1/g')
		ROUTE=$(cat sub.ipv6 | sed -e 's/$/2/g')
		# Aggiungo i comandi per il mode-sit
		echo "ip tunnel add $iface mode sit $remote local $LOCAL ttl 255" >> $iface.conf
		echo "ip link set $iface up" >> $iface.conf
		echo "ip -6 a a $ADDRESS dev $iface" >> $iface.conf
		echo "ip -6 r a $ROUTE dev $iface" >> $iface.conf
		echo "ip -6 r a $SUBNET via $ROUTE dev $iface metric 256" >> $iface.conf
		echo "sysctl -w net.ipv6.conf.$iface.forwarding=1" >> $iface.conf
		echo "" >> $iface.conf
		echo -e "# END " >> $iface.conf
		echo ""
		# Agisco sulle estensione del file finale e cancello i file inutili!
		echo -e "Converto lo script in bash per eseguirlo via ./$iface.sh"
		rm sub.ipv6
		sleep 3 
		mv $iface.conf $iface.sh
		echo -e "FATTO! Script generato: $iface.sh"
		echo -e "Ricordati di visionarlo prima di lanciarlo per verificare i dati."
		echo -e "Magari hai premuto qualche tasto in modo accidentale" 
		echo -e "Oppure sei/eri sotto l'effetto di una sostanza allucinogena... ;>"
		echo -e ""
		;;

        [nN] | [nN][Oo] )
                echo ""
		echo "Hai deciso di non continuare. Tutt APPOST!";
                echo "In ogni caso puoi visionare il prefisso generato.";
		echo "CMD: 'cat sub.ipv6'";
		exit 1
                ;;
        *)      echo ""
		echo "Scus ma nun sai legger? O vir che si GNURANT! Scegli: Y/N"
		echo "Puoi comunque visionare il prefisso generato.";
                echo "CMD: 'cat sub.ipv6'";	
                ;;
esac
