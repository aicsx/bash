# ax 6to4 tunnel NAT 
# private test version - contribute > ALL

ip tunnel del 6to4 2>/dev/null
if [ "${1}" = "stop" ]; then
  exit
fi
ip4="`wget -qO- ifconfig.co`";
# Set default route
relay=192.88.99.1
# modify localip if behind NAT. otherwise leave it empty
localip=192.168.0.2

echo $ip4 $localip
prefix=$(printf '%02x%02x:%02x%02x\n' $(echo $ip4 | sed 's/\./ /g'))
if [ -z "$localip" ]; then
  ip tunnel add 6to4 mode sit ttl 64 remote any local $ip4
else
  ip tunnel add 6to4 mode sit ttl 64 remote any local $localip
fi
ip link set dev 6to4 mtu 1280
ip link set dev 6to4 up
ip addr add 2002:$prefix::1/16 dev 6to4
ip -6 route add 2000::/3 via ::$relay dev 6to4 metric 1
