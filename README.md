# Bash scripts 

- 6to4.sh / 	
	- set 6to4 tunnel for NAT ip
- firewall.sh /
  	- status,restart,reload & build firewall script rules 
	  (incomplete! - build ip6tables no work)
- myip2.sh /
	- simple code for internal and external ipv4

- bsd-he_ipv6.sh /
	- set he.net tb allocation in NAT "FreeBSD"

- i3bar_trasp.sh /
	- set i3bar trasparent with transset

- ra-ipv6.sh /
	- random ipv6 subnet delegation

- ra-ipv6-verifier.sh /
	- random ipv6 subnet delegation and verification

#
