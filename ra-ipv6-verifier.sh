#!/bin/bash
# ra-ipv6-verifier.sh
# ax - ax[at]slackware.eu 
# release 1.0p1 

echo
echo "#	By ax			         #" 
echo "#	Sa. for			  IPV6 TB#"
echo "#	Cp. by	  ax - ax@slackware.eu	 #"
echo


# ISTRUZIONI:
# Ricordati che per rendere operativo il processo di verifica 
# tutti i file .sh generati devono essere spostati nella cartella /var/Deleghe.
# IMPORTANTE: La cartella deve avere i permessi di lettura per gruppo users!
# Setta la variabile per cambiare la cartella di destinazione.

# Lo script genera di base classi ::/80 partendo da un prefisso stabilito
# Modificare i parametri commentati successivi per cambiare il comportamento dello script!

array=( 1 2 3 4 5 6 7 8 9 0 a b c d e f ) # caratteri contenuti in indirizzi ipv6
MAXCOUNT=1 # numero di ipv6 da generare alla fine
count=1
network=2001:FFFF:a1c5:f1c4 # ipv6 network prefix
cartella=/var/Deleghe # cartella di destinazione delle deleghe
face=eth0 # interfaccia da cui prelevare l'ip local del server

echo "CIAO,"
echo "sono l'oracolo dei server *nix.."
echo "puoi chiamarmi... ax"
echo "facciamoci una chiacchierata!"
echo

rnd_ip_block ()
{
    a=${array[$RANDOM%16]}${array[$RANDOM%16]}${array[$RANDOM%16]}${array[$RANDOM%16]}

    echo $network:$a::	
}

echo "$MAXCOUNT IPv6 ::/80"
echo "*------------------------*"
while [ "$count" -le $MAXCOUNT ]        
do
        rnd_ip_block
        let "count += 1"                
        done > sub.ipv6
	cat sub.ipv6 
echo "*------------------------*" 

# istruzioni per continuare dopo!
echo ""
echo -n "ax: Vuoi creare lo script di delega con il prefisso generato? [yes or no]: "
read scelta
case $scelta in

        [yY] | [yY][Ee][Ss] )
                echo 
		echo "ax: Ok procedo, ti farò un paio di domande."
		echo 
		# Avvio script interattivo con i dati della delega 
		read -p "Quale interfaccia vuoi creare? (nome del tunnel): " iface
		read -p "Qual'è end-point? (ipv4 dell'user): " remote
		echo "ax: Perfetto! procedo con la creazione dello script"
		# Creo il file config con tutti i dati all'interno
		touch $iface.conf
		echo -e "#!/bin/bash" > $iface.conf
		echo -e "" >> $iface.conf
                # Cerco e setto la variabile per l'ipv4 locale della macchina in base alle interfacce presenti
		LOCAL=$(ip -o addr show dev $face | sed -e's/^.*inet \([^ ]*\)\/.*$/\1/;t;d' | head -1)
		ifconfig=$(ifconfig -s | awk '{ print $1 }' | grep $face | head -1)
			if [[ $ifconfig == $face || $? == $0 ]]; then
                                echo 
                                echo "local addr: [$LOCAL]"
                        else
                                echo 
                                echo "ATTENZIONE: L'interfaccia local $face non esiste!"
                                echo "Non posso continuare a generare la delega..."
				echo "In ogni caso puoi visionare il prefisso generato.";
                		echo "CMD: 'cat sub.ipv6'";
				echo "# End"
				echo
				exit 1
                        fi  	
		# Aggiungo le variabili alla fine della stringa per la subnet creata in precedenza
		SUBNET=$(cat sub.ipv6 | sed -e 's/$/\/80/g')
		ADDRESS=$(cat sub.ipv6 | sed -e 's/$/1/g')
		ROUTE=$(cat sub.ipv6 | sed -e 's/$/2/g')
		# Aggiungo i comandi per il mode-sit che andranno nello script
		echo "ip tunnel add $iface mode sit remote $remote local $LOCAL ttl 255" >> $iface.conf
		echo "ip link set $iface up" >> $iface.conf
		echo "ip -6 a a $ADDRESS dev $iface" >> $iface.conf
		#echo "ip -6 r a $ROUTE dev $iface" >> $iface.conf
		#echo "ip -6 r a $SUBNET via $ROUTE dev $iface metric 256" >> $iface.conf
		echo "ip -6 r a $SUBNET dev $iface metric 256" >> $iface.conf
		echo "sysctl -w net.ipv6.conf.$iface.forwarding=1" >> $iface.conf
		echo "" >> $iface.conf
		echo -e "# END " >> $iface.conf
		echo 
		# Agisco sulle estensioni del file finale!
		echo -e "ax: converto lo script in bash per eseguirlo via ./$iface.sh" 
		sleep 3
		mv $iface.conf $iface.sh
		echo 
		echo -e "ax: FATTO! Script generato: $iface.sh"
		echo 
		echo -e "Ricordati di visionarlo prima di lanciarlo per verificare i dati."
		echo -e "Magari hai premuto qualche tasto in modo accidentale..." 
		echo -e "Oppure sei/eri sotto l'effetto di una sostanza allucinogena... ;>"
		# creo lo script da dare all'utente
                touch $iface.user-conf
                echo -e "#!/bin/bash" > $iface.user-conf
                echo "" >> $iface.user-conf
                echo -e "ip tunnel add $iface mode sit remote $LOCAL local $remote ttl 255" >> $iface.user-conf
                echo -e "ip link set $iface up" >> $iface.user-conf
                echo -e "ip -6 a a $ROUTE/80 dev $iface" >> $iface.user-conf
                echo -e "ip -6 r a $ADDRESS dev $iface" >> $iface.user-conf
                echo -e "ip -6 r a default via $ADDRESS  dev $iface metric 256" >> $iface.user-conf       
                echo -e "sysctl -w net.ipv6.conf.$iface.forwarding=1" >> $iface.user-conf
                echo "" >> $iface.user-conf
                echo -e "# END " >> $iface.user-conf
                echo -e "Ho creato lo script client per l'utente finale [$iface.user-conf]"            
                echo
		;;

        [nN] | [nN][Oo] )
                echo 
		echo "ax: Hai deciso di non continuare. Tutt APPOST!";
                echo "In ogni caso puoi visionare il prefisso generato.";
		echo "CMD: 'cat sub.ipv6'";
		exit 1
                ;;
        *)      echo 
		echo "ax: Scus ma nun sai legger? O vir che si GNURANT! Scegli: Y/N"
		echo "Puoi comunque visionare il prefisso generato.";
                echo "CMD: 'cat sub.ipv6'";	
		exit 1
		;; 
esac

# istruzioni per la verifica del suffisso generato!
echo 
echo -n "ax: Vuoi verificare se la subnet creata esiste già? [yes or no]: "
read verifica
case $verifica in

        [yY] | [yY][Ee][Ss] )
		echo ""
		echo "ax: OK procedo con la verifica."
		# Riprendo i parametri della subnet generata e effettuo la verifica nella directory specificata
		VERIFY=$(grep -r -i "$SUBNET" $cartella | awk '{ print $5 }')
			if [ "$SUBNET" == "$VERIFY" ]; then
              			echo 
				echo "ATTENZIONE: la subnet creata è già attiva"
				echo "Premi un tasto per continuare ..."
				read # blocco il continuo dello script fino alla selezione di un tasto a caso
				echo 
				echo "ax: Le probabilità dovevano essere rare in teoria."
				echo "A quanto pare, invece, l'improbabile è dietro l'angolo..."
				echo "Devi necessariamente ricominciare il processo di creazione della subnet!"
				echo "Lo script successivo potrebbe tornarti utile..."
				echo 
			else
             			echo 
				echo "ax: Nessuna subnet con lo stesso prefisso ;>"
				echo "Vai liscio come l'olio! PROCEDI!"
			fi
		rm sub.ipv6 
		;;
	[nN] | [nN][Oo] )
                echo 
		echo "ax: Hai deciso di fregartene..."
                echo "Se esiste già: cazzi tuoi!"
		echo "In ogni caso puoi visionare lo script *.sh generato.";
                rm sub.ipv6
		exit 1
                ;;
        *)      echo 
                echo "ax: Tasto non consentito ..."
		echo "Taviss mbarà a legger nu poc meglio..."
		echo "In ogni caso puoi visionare lo script *.sh generato.";
		echo "ADDIO!"
                rm sub.ipv6
		exit 1
		;; 
esac

# Scelta delle azioni finali sul file.sh generato!
echo
PS3="ax: Scegli se/come agire su [$iface.sh] oppure premi [ CTRL+C ] per terminare lo script: "
select azione in "Visualizza" "Cambia permessi" "Sposta" "Elimina" "Esci"; do

	case $azione in
		"Visualizza")
			echo
			echo "ax: informazioni pe GNURANT! :"
			echo "Tasti direzionali [◀▼▲▶ | pagSU-pagGIU]"
			echo "Quit [q]"
			echo "Premi un tasto per continuare nella visualizzazione..."
			read
			less $iface.sh
			echo
			;; 
		"Cambia permessi")
			echo
			chmod +x $iface.sh
			echo "ax: Ho cambiato i permessi di [$iface.sh] per l'esecuzione."
			echo "Adesso puoi lanciarlo con [sudo|su ./$iface.sh]"
			echo
			;;
		"Sposta")
			echo "ax: Verifico se ci sono le condizioni per spostare il file..."
			echo
			# setto varibile di controllo dell'esistenza del file nella directory di destinazione
			if [[ -f $cartella/$iface.sh ]]; then # se esiste il file $cartella/$iface.sh fai questo 
                                echo "ATTENZIONE: il file $iface.sh esiste già!"
                                echo "Probabilmente hai già assegnato una delega con questo nome."
                                echo "Non posso copiare il file per ragioni di sicurezza!."
				echo "A questo punto ti consiglio di eliminarlo prima di uscire."
                                echo 
				elif [[ -x $iface.sh  ]]; then                                                                                                 
                                        mv $iface.sh $cartella
                                        echo "ax: $iface.sh è stato spostato nella dir delle deleghe: [$cartella]"
                                        echo
                                        exit 1
                                else
                                        echo "ax: $iface.sh non è eseguibile!"
                                        echo "Devi prima cambiare i permessi."
                                        echo 
                                fi  
			;; 
		"Elimina")
			rm $iface.sh
			echo "ax: ho eliminato [$iface.sh]"
			echo
			exit 1
			;;
		"Esci")
			echo "ax: ADDIO"
			exit 1
			;;
	esac
done
